import clsx from 'clsx';
import { PropsWithChildren } from 'react';

interface ErrorMsgProps {
  icon?: string;
}

export function ErrorMsg(props: PropsWithChildren<ErrorMsgProps>) {
  return props.children ?
    <div className="alert alert-danger">
      {props.icon  && <i className={clsx(props.icon, 'mx-1')}></i>}
      {props.children}
    </div> : null
}
