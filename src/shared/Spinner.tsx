export function Spinner() {
  return (
    <div>
      <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
  )
}
