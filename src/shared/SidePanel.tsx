import clsx from 'clsx';
import { PropsWithChildren, useEffect } from 'react';
import css from './SidePanel.module.css';

interface SidePanelProps {
  title: string;
  open: boolean;
  onClose: () => void;
}

export function SidePanel(props: PropsWithChildren<SidePanelProps>) {

  function save(){
    // ...
    props.onClose();
  }
  return (
    <div
      className={clsx(
        css.sidepanel,
        'shadow-lg',
        { [css.open]: props.open }
      )}
    >
      <h1>{props.title}</h1>

      {props.children}

      <button
        onClick={save}
      >SAVE</button>

    </div>
  )
}
