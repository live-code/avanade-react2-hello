import clsx from 'clsx';
import React, { PropsWithChildren } from 'react';
import styles from './Title.module.css';

interface CardProps {
  title: string;
  img?: string;
  type?: 'failed' | 'success';
  buttonLabel?: string;
  buttonUrl?: string;
  buttonTarget?: '_self' | '_blank';
  icon?: string;
  onIconClick?: () => void;
}
export function Card(props: PropsWithChildren<CardProps>) {
  const {
    title, icon, type, img, buttonTarget = '_blank', buttonLabel, buttonUrl, children,
    onIconClick
  } = props;


  return (
    <div className={clsx(
      'card',
      { 'bg-danger': type === 'failed' },
      { 'bg-success': type === 'success' },
    )}>
      {img && <img className="card-img-top" src={img} alt={title}/>}
        <div className="card-body">
          <h5 className="card-title">
            {
              icon &&
                <i
                  className={icon}
                  onClick={onIconClick}
                ></i>}
            {title}
          </h5>
          <div className="my-3">
            {children}
          </div>

          {
            buttonLabel &&
              <div className="d-flex justify-content-end">
                <a
                  href={buttonUrl}
                  target={buttonTarget}
                  className="btn btn-primary"
                >
                  {buttonLabel}
                </a>
              </div>
          }
        </div>
    </div>
  )
}
