import { PropsWithChildren } from 'react';
import styles from './Title.module.css';

export function Title(props: PropsWithChildren) {
  return <h1 className={styles.title}>
    {props.children}
  </h1>
}
