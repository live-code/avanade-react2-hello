import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { NavBar } from './core/NavBar';

import { MyRoutes } from './Routes';

export default function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <hr/>
      <MyRoutes />
    </BrowserRouter>
  )
}


