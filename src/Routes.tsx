import React, { lazy, Suspense } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import DemoCounter from './pages/demo-counter/DemoCounter';
import DemoList from './pages/demo-list/DemoList';
import { Details } from './pages/demo-list/Details';
// import DynamicComponents from './pages/demos/DynamicComponents';
import { UseEffectDemo } from './pages/demos/UseEffectDemo';
import { Form1Demo } from './pages/forms/Form1Demo';
import { Form2Demo } from './pages/forms/Form2Demo';
import { Form3Demo } from './pages/forms/Form3Demo';
import Home from './pages/home/Home';
import { Login } from './pages/login/Login';
import { LostPass } from './pages/login/pages/LostPass';
import { Registration } from './pages/login/pages/Registration';
import { SignIn } from './pages/login/pages/SignIn';
// import { UikitDemo } from './pages/uikit/UikitDemo';

const UikitDemo = lazy(() => import('./pages/uikit/UikitDemo'))
const DynamicComponents = lazy(() => import('./pages/demos/DynamicComponents'))

export function MyRoutes() {
  return (
    <Routes>
      <Route
        path="login"
        element={<Login />}
      >
        <Route index element={<SignIn />} />
        <Route path="registration" element={<Registration />} />
        <Route path="lostpass" element={<LostPass />} />
      </Route>

      <Route path="counter" element={<DemoCounter />} />
      <Route path="list" element={<DemoList />} />
      <Route path="list/:userId" element={<Details />} />

      <Route path="uikit" element={
        <Suspense fallback={<div>loading uikit...</div>}>
          <UikitDemo />
        </Suspense>
      } />

      <Route path="useEffect" element={<UseEffectDemo />} />
      <Route path="dynamic-compo" element={
        <Suspense fallback={<div>loading...</div>}>
          <DynamicComponents />
        </Suspense>
      } />
      <Route path="form1" element={<Form1Demo />} />
      <Route path="form2" element={<Form2Demo />} />
      <Route path="form3" element={<Form3Demo />} />
      <Route path="" element={<Home />} />
      <Route path="*" element={<Navigate to="/" />} />
    </Routes>
  )
}
