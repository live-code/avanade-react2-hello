import { Link, Outlet } from 'react-router-dom';

export function Login() {
  return (
    <div>
     <h1> login </h1>

      <Outlet />

      <hr/>

      <Link to="">sign in</Link>
      <Link to="registration">registration</Link>
      <Link to="/login/lostpass">lost pass</Link>
    </div>
  )
}
