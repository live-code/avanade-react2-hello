interface TotalProps {
  title: string;
  total: number;
}

export const Total = ({ title, total }: TotalProps) => {

  return (
    <div>
      <h1>{title} </h1>
      <div>There are {total} products</div>
    </div>
  )

}
