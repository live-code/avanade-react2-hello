import React, { useState } from 'react';
import { Empty } from './components/Empty'
import { Total } from './components/Total';

export default function DemoCounter() {
  // destructuring JS array / obj
  const [totalProducts, setTotalProducts] = useState(0)

  function reset() {
    setTotalProducts(5);
  }

  function inc() {
    // setTotalProducts(totalProducts + 1);
    setTotalProducts(totalProducts + 1);
  }

  function dec() {
    setTotalProducts((prevState) => prevState - 1);
  }

  return (
    <>
      {
        totalProducts ?
          <Total title="Totale prodotti" total={totalProducts}/> :
          <Empty />
      }

      <button onClick={dec}>-</button>
      <button onClick={inc}>+</button>
      <button onClick={reset}>reset</button>
    </>
  )
}

/*

btn.addEventListener('click', inc)

function inc(e) {
  console.log(e.)
}*/
