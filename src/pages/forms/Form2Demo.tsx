import React, { useRef, useState } from 'react';

export function Form2Demo() {
  const [users, setUsers] = useState<any[]>([])

  const input1 = useRef<HTMLInputElement>(null);
  const input2 = useRef<HTMLInputElement>(null);

  console.log('render')

  function submitHandler(e: React.FormEvent<HTMLFormElement>) {
      e.preventDefault();
      const input1Value = input1.current?.value
      const input2Value = input2.current?.value

      if (
        (input1Value && input1Value.length > 3) &&
        (input2Value && input2Value.length > 3)
      ) {
        const user = {
          name: input1Value,
          surname: input2Value,
        }
        setUsers([...users, user])
      }

  }

  return <div>
    <h1>Uncontrolled Form</h1>

    <form onSubmit={submitHandler}>
      <input ref={input1} type="text" name="name" />
      <input ref={input2} type="text" name="surname" />
      <button type="submit" >SAVE</button>
    </form>
    <pre>{JSON.stringify(users, null, 2)}</pre>

  </div>
}
