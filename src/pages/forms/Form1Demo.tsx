import React, { useState } from 'react';

export function Form1Demo() {
  const [users, setUsers] = useState<any[]>([])

  function changeHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      setUsers([...users, { name: e.currentTarget.value }])
      e.currentTarget.value = '';
    }
  }
  return <div>
    <h1>Form 1 demo</h1>

    <input type="text" onKeyDown={changeHandler} placeholder="name"/>
    <input type="text" onKeyDown={changeHandler} placeholder="surname"/>

    <pre>{JSON.stringify(users, null, 2)}</pre>
  </div>
}
