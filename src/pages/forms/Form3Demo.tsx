import clsx from 'clsx';
import React, { useState } from 'react';

export function Form3Demo() {
  const [formData, setFormData] = useState({name: '', surname: '', msg: '', subscribe: false, gender: '', rate: 0});
  const [dirty, setDirty] = useState(false)

  function changeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) {
    const key = e.currentTarget.name;
    const value = e.currentTarget.value;
    setFormData({ ...formData, [key]: value });
    setDirty(true)
  }

  function changeCheckboxHandler(e: React.ChangeEvent<HTMLInputElement>) {
    const key = e.currentTarget.name;
    const value = e.currentTarget.checked;
    setFormData({ ...formData, [key]: value });
    setDirty(true)
  }

  function submitHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    console.log(formData)
  }

  const isNameValid = formData.name.length > 1;
  const isSurnameValid = formData.surname.length > 1;
  const isGenderValid = formData.gender !== ''
  const isValid = isNameValid && isSurnameValid && isGenderValid

  return <div>
    <h1>Controlled Form</h1>

    <form onSubmit={submitHandler}>
      <input
        type="range" name="rate" min="0" max="10"
        value={formData.rate}
        onChange={changeHandler}
      />
      <input
        className={clsx('form-control', { 'is-invalid': !isNameValid && dirty, 'is-valid': isNameValid })}
        name="name" type="text"
        value={formData.name}
        onChange={changeHandler}
      />
      <input
        className={clsx('form-control', { 'is-invalid': !isSurnameValid && dirty, 'is-valid': isSurnameValid })}
        name="surname" type="text" value={formData.surname}
        onChange={changeHandler}
      />
      <input
        name="subscribe" type="checkbox"
        checked={formData.subscribe}
        onChange={changeCheckboxHandler}
      />

      <textarea
        className="form-control"
        onChange={changeHandler}
        value={formData.msg}
        name="msg"
      ></textarea>

      <select
        className={clsx('form-control', { 'is-invalid': !isGenderValid && dirty, 'is-valid': isGenderValid })}
        value={formData.gender} onChange={changeHandler} name="gender">
        <option value="">Select Gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>

      <button type="submit" disabled={!isValid}>save</button>
    </form>

    <pre>{JSON.stringify(formData, null, 2)}</pre>
  </div>
}
