import { useState } from 'react';
import { Card } from '../../shared/Card';
import { SidePanel } from '../../shared/SidePanel';
import { Title } from '../../shared/Title';

export default function UikitDemo() {
  const [visible, setVisible] = useState(false);

  function doSomething() {
    setVisible(true)
  }

  return <div>
    <Title>Ui kit page</Title>

    <button onClick={() => setVisible(true)}>Open Panel</button>
    <button onClick={() => setVisible(false)}>Close Panel</button>

    <SidePanel
      open={visible}
      title="SIDE PANEL"
      onClose={() => setVisible(false)}
    >
      <input type="text"/>
      <input type="text"/>
      <input type="text"/>
    </SidePanel>
    <Card
      type="failed"
      img="https://cdn.pixabay.com/photo/2012/08/27/14/19/mountains-55067__340.png"
      icon="fa fa-link"
      onIconClick={doSomething}
      title="Super Card 1"
      buttonLabel="Visit website"
      buttonUrl="https://www.google.com"
    >
      <input type="text"/>
      <input type="text"/>
      <input type="text"/>

    </Card>

    <Card title="Ok! Done">
      bla bla success
    </Card>

  </div>
}
