import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../model/user';
import { ErrorMsg } from '../../shared/ErrorMsg';
import { Spinner } from '../../shared/Spinner';
import { UsersList } from './components/UsersList';

export default function DemoList() {
  const [users, setUsers] = useState<User[]>([]);
  const [pending, setPending] = useState(false);
  const [error, setError] = useState<string | null>(null)

  useEffect(() => {
    loadData();
  }, [])

  function loadData() {
    setPending(true);
    axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then((res) => setUsers(res.data))
      .catch(err => setError(err.message))
      .finally(() => setPending(false))

  }

  function addUser() {
    setError(null);
    const formdata = { name: 'Fabio'};
    axios.post<User>(`https://jsonplaceholder.typicode.com/users/`, formdata)
      .then(res => setUsers([...users, res.data]))
      .catch(err => setError(err.message))
  }

  function deleteUser(id: number) {
    setError(null);
    axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(() => {
        const newUsers = users.filter(u => u.id !== id)
        setUsers(newUsers)
      })
      .catch(err => {
        setError(err.message);
      })
  }


  return (
    <div>
      <ErrorMsg icon="fa fa-exclamation-triangle">{error}</ErrorMsg>
      {pending && <Spinner />}

      {
        users.length === 0 ?
          <NoUsers /> :
          <UsersList
            items={users}
            onDeleteUser={deleteUser}
          />
      }

      <button onClick={addUser}>Add user</button>

    </div>
  )
}


export function NoUsers() {
  return <div>no users</div>
}

