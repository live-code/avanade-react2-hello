import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { User } from '../../model/user';




export function Details() {
  const params = useParams();
  const [user,setUser] = useState<Partial<User> | null>({ name: ''})

  useEffect(() => {
    axios.get<User>(`https://jsonplaceholder.typicode.com/users/${params.userId}`)
      .then(res => {
        setUser(res.data);
      })
  }, [])

  function changeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setUser({...user, name: e.currentTarget.value})
  }

  return (
    <div>
      <h1>{user?.name}</h1>
      <h1>{user?.email}</h1>
      <h1>{user?.phone}</h1>

      <input type="text" value={user?.name} onChange={changeHandler}/>


      <hr/>
      <Link to="/list">Back to list</Link>
    </div>
  )
}
