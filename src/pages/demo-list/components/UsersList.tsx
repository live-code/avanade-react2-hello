// STATELESS - PRESENTATIONAL COMPONENTS - DUMB COMPONENT
import { Link } from 'react-router-dom';
import { User } from '../../../model/user';

interface UsersListProps {
  items: User[];
  onDeleteUser: (id: number) => void;
}
export function UsersList(props: UsersListProps) {
  return <div>
    {
      props.items.map(user => {
        return (
          <li>
            <Link to={'/list/' + user.id} key={user.id}>
              {user.name} ({user.id})
            </Link>
            <button onClick={() => props.onDeleteUser(user.id)}>DEL</button>

          </li>
        )
      })
    }
  </div>
}
