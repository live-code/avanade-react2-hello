import { User } from '../../../model/user';

interface ListProps {
  users: User[];
  onItemClick: (id: number) => void;
}
export function List(props: ListProps) {
  return (
    <ul>
      {
        props.users.map(user => {
          return <li onClick={() => props.onItemClick(user.id)} key={user.id}>{user.name}</li>
        })
      }
    </ul>

  )
}
