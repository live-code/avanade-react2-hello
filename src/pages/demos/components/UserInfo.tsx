import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../../model/user';

interface UserInfoProps {
  id: number;
}
export function UserInfo(props: UserInfoProps) {
  const [user, setUser] = useState<User | null>(null)

  useEffect(() => {
    axios.get<User>(`https://jsonplaceholder.typicode.com/users/${props.id}`)
      .then(res => {
        setUser(res.data)
      })
  }, [props.id])

  return (
    <div className="alert alert-info">
      {user?.name}
      {user?.email}
    </div>
  )
}
