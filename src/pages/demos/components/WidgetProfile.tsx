import { PropsWithChildren } from 'react';

interface WidgetProfileProps {
  title: string;
}
const WidgetProfile = (props: PropsWithChildren<WidgetProfileProps>) => <div>
  <h1>PROFILE: {props.title}</h1>

  <div>
    Body: {props.children}
  </div>
</div>

export default WidgetProfile;
