interface WidgetChartProps {
  items: number[];
}

export default function WidgetChart(props: WidgetChartProps) {
  return <div>
    <h1>Chart</h1>
    {props.items}
  </div>
}
