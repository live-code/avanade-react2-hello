import axios from 'axios';
import { useEffect, useState } from 'react';
import { User } from '../../model/user';
import { Title } from '../../shared/Title';
import { List } from './components/List';
import { UserInfo } from './components/UserInfo';

export function UseEffectDemo() {
  const [users, setUsers] = useState<User[]>([])
  const [selectedUserId, setSelectedUserId] = useState<number | null>(null)

  useEffect(() => {
    const controller = new AbortController();

    axios.get<User[]>('https://jsonplaceholder.typicode.com/users', {
      signal: controller.signal
    })
      .then(res => {
        setUsers(res.data)
      })

    // cleanup
    return () => {
      controller.abort()
      console.log('DESTROY')
    }
  }, [])

  return (
    <div>
      <Title>Use effect demo</Title>

      <List users={users} onItemClick={setSelectedUserId} />

      { selectedUserId && <UserInfo id={selectedUserId} /> }
    </div>
  )
}
