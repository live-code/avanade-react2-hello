import React, { FunctionComponent, Suspense, useState } from 'react';
// import { WidgetChart } from './components/WidgetChart';
// import { WidgetProfile } from './components/WidgetProfile';
const WidgetChart = React.lazy(() => import('./components/WidgetChart'))
const WidgetProfile = React.lazy(() => import('./components/WidgetProfile'))

const COMPONENTS: { [key: string]: FunctionComponent<any> } = {
  'chart': WidgetChart,
  'profile': WidgetProfile,
}


// JSON DATA
const dashboard: { component: string, data: any}[] = [
  {
    component: 'profile',
    data: { title: 'John Doe', color: 'red', children: 'SOME CONTENT HERE!' }
  },
  {
    component: 'chart',
    data: { id: 2, title: 'I am a chart!', items: [10, 20, 30] }
  },
]

export default function DynamicComponents() {

  return <div>
    {
      dashboard.map(item =>  {
        const compo = React.createElement(COMPONENTS[item.component], item.data );
        return <Suspense fallback={<div>loading component...</div>}>
          {compo}
        </Suspense>
      })
    }
  </div>
}
