
import React from 'react';
import { Page } from './model/page';
import DemoCounter from './pages/demo-counter/DemoCounter';
import DemoList from './pages/demo-list/DemoList';
import { UseEffectDemo } from './pages/demos/UseEffectDemo';
import { Form1Demo } from './pages/forms/Form1Demo';
import { Form2Demo } from './pages/forms/Form2Demo';
import { Form3Demo } from './pages/forms/Form3Demo';
import Home from './pages/home/Home';
import UikitDemo from './pages/uikit/UikitDemo';

interface PageSwitchProps {
  value: Page
}

export function PageSwitch(props: PageSwitchProps) {
  return <div className="m-3">
    {PAGES[props.value]}
  </div>
}


export const PAGES: {[key: string]: React.ReactNode} = {
  'home': <Home />,
  'counter': <DemoCounter />,
  'list': <DemoList />,
  'uikit': <UikitDemo />,
  'use-effect': <UseEffectDemo />,
  'form1': <Form1Demo />,
  'form2': <Form2Demo />,
  'form3': <Form3Demo />,
}
