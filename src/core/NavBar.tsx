import React from 'react';
import { NavLink } from 'react-router-dom';


const isActiveHandler = (obj: { isActive: boolean} ) => {
  return obj.isActive ? 'btn btn-primary bg-dark' : 'btn btn-primary'
}

export function NavBar() {
  return (
    <div className="bg-dark btn-group p-3">
      <NavLink className={isActiveHandler} to="login">login</NavLink>
      <NavLink className={isActiveHandler} to="">home</NavLink>
      <NavLink className={isActiveHandler} to="counter" >counter</NavLink>
      <NavLink className={isActiveHandler} to="list">list</NavLink>
      <NavLink className={isActiveHandler} to="uikit">UIKIT</NavLink>
      <NavLink className={isActiveHandler} to="dynamic-compo">dynamic compo</NavLink>
      <NavLink className={isActiveHandler} to="useEffect">Use Effect</NavLink>
      <NavLink className={isActiveHandler} to="form1">Form 1</NavLink>
      <NavLink className={isActiveHandler} to="form2">Form 2</NavLink>
      <NavLink className={isActiveHandler} to="form3">Form 3</NavLink>
    </div>
  )
}
